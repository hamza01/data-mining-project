#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from R_square_clustering import r_square
from purity import purity_score
from scipy.cluster.hierarchy import dendrogram, linkage




data = pd.read_csv("covid19/covid19.csv", sep=",")
df = pd.DataFrame(data)

#Q2

# dff = pd.DataFrame(data[{'vaccine_2021-winter', 'vaccine_2021-spring', 'vaccine_2021-summer' , 'vaccine_2021-fall', 'vaccine_2022-winter'}])
# dff['total_dose'] = dff.sum(axis=1)
# mille = dff['total_dose'] * 1000
# data["dose_for_1000"] = mille / data['population']

# print(df.head())

data['dose_for_1000'] = ((data['vaccine_2021-winter'] + data['vaccine_2021-spring'] + data['vaccine_2021-summer'] + data['vaccine_2021-fall'] + data['vaccine_2022-winter']) * 1000 ) / data['population']

data['vaccine_2021-winter'] = (data['vaccine_2021-winter'] * 1000) / data['population']
data['vaccine_2021-spring'] = (data['vaccine_2021-spring'] * 1000) / data['population']
data['vaccine_2021-summer'] = (data['vaccine_2021-summer'] * 1000) / data['population']
data['vaccine_2021-fall'] = (data['vaccine_2021-fall'] * 1000) / data['population']
data['vaccine_2022-winter'] = (data['vaccine_2022-winter'] * 1000) / data['population']


# print(df.head())
data.pop("population")
# print(df.head())

#Q3

scaler = StandardScaler()
x = data.iloc[:,2:]
y = data['country_code']
X_norm = scaler.fit_transform(x)
x.describe()
#Q4

acp = PCA(svd_solver='full')
coord = acp.fit_transform(X_norm)
n = np.size(X_norm, 0)
print("----------------")
print(acp.n_components_) 


#ACP facteur 1,2
fig, axes = plt.subplots(figsize=(12,12))
axes.set_xlim(-9,9)
axes.set_ylim(-4,4)
for i in range(n):
    plt.annotate(y.values[i],(coord[i,0],coord[i,1]))
plt.plot([-10,10],[0,0],color='red',linestyle='-',linewidth=1)
plt.plot([0,0],[-5,5],color='red',linestyle='-',linewidth=1)
plt.savefig('acp/acp_1_2')
plt.close(fig)


# #ACP facteur 3,4
fig, axes = plt.subplots(figsize=(12,12))
axes.set_xlim(-9,9)
axes.set_ylim(-4,4)
for i in range(n):
    plt.annotate(y.values[i],(coord[i,2],coord[i,3]))
plt.plot([-10,10],[0,0],color='red',linestyle='-',linewidth=1)
plt.plot([0,0],[-5,5],color='red',linestyle='-',linewidth=1)
plt.savefig('acp/acp_3_4')
plt.close(fig)


#Q5

n = np.size(X_norm, 0)
p = np.size(X_norm, 1)
eigval = float(n-1)/n*acp.explained_variance_
sqrt_eigval = np.sqrt(eigval)
corvar = np.zeros((p,p))
for k in range(p):
    corvar[:,k] = acp.components_[k,:] * sqrt_eigval[k]
pd.DataFrame(corvar, list(data.columns[2:])).head()


# plot correlation_circles
def correlation_circle(var_names,nb_var,x_axis,y_axis):
    fig, axes = plt.subplots(figsize=(8,8))
    minx = -1
    maxx = 1
    miny = -1
    maxy = 1
    axes.set_xlim(minx,maxx)
    axes.set_ylim(miny,maxy)
    for j in range(nb_var):
        plt.annotate(var_names[j+2],(corvar[j,x_axis],corvar[j,y_axis]))
        axes.arrow(0, 0, corvar[j,x_axis],corvar[j,y_axis], head_width=0.01, head_length=0.02, fc='k', ec='k')
    # axes
    plt.plot([minx,maxx],[0,0],color='silver',linestyle='-',linewidth=1)
    plt.plot([0,0],[miny,maxy],color='silver',linestyle='-',linewidth=1)
    # add a circle
    cercle = plt.Circle((0,0),1,color='blue',fill=False)
    axes.add_artist(cercle)
    plt.savefig('fig/acp_correlation_circle_axes_'+str(x_axis)+'_'+str(y_axis))
    plt.close(fig)

correlation_circle(data.columns,p,0,1)
correlation_circle(data.columns,p,2,3)

#Q6

lst_k=range(2,20)
lst_rsq = []
for k in lst_k:
    est=KMeans(n_clusters=k)
    est.fit(X_norm)
    lst_rsq.append(r_square(X_norm, est.cluster_centers_,est.labels_,k))

fig = plt.figure()
plt.plot(lst_k, lst_rsq, 'bx-')
plt.xlabel('k')
plt.ylabel('RSQ')
plt.title('The Elbow Method showing the optimal k')
plt.savefig('acp/k-means_elbow_method')
plt.close()

#Q7 k = 8

# print centroids associated with several countries
lst_countries=['FR','DK','BG']
est=KMeans(8)
est.fit(X_norm)
# print(est.cluster_centers_)
for name in lst_countries:
    num_cluster = est.labels_[y.loc[y==name].index][0]
    print('Num cluster for '+name+': '+str(num_cluster))
    print('\tlist of countries: '+', '.join(y.iloc[np.where(est.labels_==num_cluster)].values))
    print('\tcentroid: '+str(est.cluster_centers_[num_cluster]))


#Q8

lst_labels = map(lambda pair: pair[0]+str(pair[1]), zip(data['country_code'].values,data.index))
linkage_matrix = linkage(X_norm, 'ward')
fig = plt.figure()
dendrogram(
    linkage_matrix,
    labels=list(lst_labels),
)
plt.title('Hierarchical Clustering Dendrogram (Ward)')
plt.xlabel('sample index')
plt.ylabel('distance')
plt.tight_layout()
plt.savefig('fig/hierarchical-clustering')
plt.close()