#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from sklearn.dummy import DummyClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn import metrics
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder



scaler = StandardScaler()

#Q1 , 2 et 3 : --------------

labor = pd.read_csv("labor/labor.csv", sep=",", na_values='?')
df = pd.DataFrame(labor)
print (labor)
print (labor.shape)
# print (data.describe())
print(df["class"].value_counts())



data_num = df.select_dtypes(include='float64')


#Q4 : --------------

imputer = SimpleImputer(missing_values=np.nan, strategy='mean')

data_normal = scaler.fit_transform(data_num)

X = imputer.fit_transform(data_normal)

y = df['class']



dummycl = DummyClassifier(strategy="most_frequent")
gmb = GaussianNB()
dectree = tree.DecisionTreeClassifier()
rdforest = RandomForestClassifier()
logreg = LogisticRegression(solver="liblinear")
svc = svm.SVC(gamma='scale')

lst_classif = [dummycl, gmb, dectree, rdforest, logreg, svc]
lst_classif_names = ['Dummy', 'Naive Bayes', 'Decision tree', 'Random Forest', 'Logistic regression', 'SVM']

def accuracy_score(lst_classif,lst_classif_names,X,y):
    for clf,name_clf in zip(lst_classif,lst_classif_names):
        scores = cross_val_score(clf, X, y, cv=5)
        print("Accuracy of "+name_clf+" classifier on cross-validation: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

def confusion_matrix(lst_classif,lst_classif_names,X,y):
    for clf,name_clf in zip(lst_classif,lst_classif_names):
        predicted = cross_val_predict(clf, X, y, cv=5) 
        print("Accuracy of "+name_clf+" classifier on cross-validation: %0.2f" % metrics.accuracy_score(y, predicted))
        print(metrics.confusion_matrix(y, predicted))

# print("accuracy_score : ")
# accuracy_score(lst_classif,lst_classif_names,X,y)

#Q5 : --------------

# print("confusion_matrix : ")
# confusion_matrix(lst_classif,lst_classif_names,X,y)

#Q6 : --------------
        

# Replace missing values by mean and discretize categorical values
data_cat = df.select_dtypes(exclude='float64').drop('class',axis=1)

# datatype = df.dtypes
# tmp3 = datatype[(datatype == 'object') | (datatype == 'category')].index.tolist()
# print("name ---------------",tmp3)

# imputer_cat = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
imputer_cat = SimpleImputer(missing_values=np.nan, strategy='constant', fill_value='UNK')

X_cat = imputer_cat.fit_transform(data_cat)

# Disjonction with OneHotEncoder
encoder = OneHotEncoder(handle_unknown="ignore")
encoder.fit(X_cat)
X_cat = encoder.transform(X_cat).toarray()




def accuracy_score_cat(lst_classif,lst_classif_names,X,y):
    for clf,name_clf in zip(lst_classif,lst_classif_names):
        scores = cross_val_score(clf, X, y, cv=5)
        print("Accuracy of "+name_clf+" classifier on cross-validation: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

def confusion_matrix_cat(lst_classif,lst_classif_names,X,y):
    for clf,name_clf in zip(lst_classif,lst_classif_names):
        predicted = cross_val_predict(clf, X, y, cv=5) 
        print("Accuracy of "+name_clf+" classifier on cross-validation: %0.2f" % metrics.accuracy_score(y, predicted))
        print(metrics.confusion_matrix(y, predicted))

# print("accuracy_score_cat : ")
# accuracy_score_cat(lst_classif,lst_classif_names,X_cat,y)
# print("confusion_matrix_cat : ")
# confusion_matrix_cat(lst_classif,lst_classif_names,X_cat,y)


#Q7 : --------------


X_all =  np.concatenate((X_cat,X),axis=1)

def accuracy_score_all(lst_classif,lst_classif_names,X,y):
    for clf,name_clf in zip(lst_classif,lst_classif_names):
        scores = cross_val_score(clf, X, y, cv=5)
        print("Accuracy of "+name_clf+" classifier on cross-validation: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

def confusion_matrix_all(lst_classif,lst_classif_names,X,y):
    for clf,name_clf in zip(lst_classif,lst_classif_names):
        predicted = cross_val_predict(clf, X, y, cv=5) 
        print("Accuracy of "+name_clf+" classifier on cross-validation: %0.2f" % metrics.accuracy_score(y, predicted))
        print(metrics.confusion_matrix(y, predicted))

# print("accuracy_score_all : ")
# accuracy_score_all(lst_classif,lst_classif_names,X_all,y.values)
# print("confusion_matrix_all : ")
# confusion_matrix_all(lst_classif,lst_classif_names,X_all,y.values)