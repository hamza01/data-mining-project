Il est recommandé d'utiliser Python 3 pour ces TP.

Pour utiliser les bibliothèques Python requises pour ce TP, il est conseillé d'installer Anaconda.
Pour limiter la taille des paquets installés sur votre compte, vous pouvez plutôt télécharger Miniconda.

L'installation des paquets dans votre espace Anaconda se fait comme suit :
conda install pandas numpy scikit-learn
conda install matplotlib

Bien vérifier que la version python d'anaconda est lancée au démarrage :
Python 3.8.3 (default, May 19 2020, 18:47:26)
[GCC 7.3.0] :: Anaconda, Inc. on linux

Si ce n'est pas le cas, ajouter le répertoire <INSTALL-DIR>/miniconda3/bin dans le PATH
