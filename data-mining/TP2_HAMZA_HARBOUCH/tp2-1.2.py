#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import arff


# convert categorical values into one-hot vectors and ignore ? values
# corresponding to missing values
# pd.set_option('display.max_columns', None)
data=arff.loadarff('supermarket.arff')
supermarket = pd.DataFrame(data[0])

# print(supermarket.head())

# option to show all itemsets
pd.set_option('display.max_colwidth',-1)

supermarket_one_hot = pd.get_dummies(supermarket)
supermarket_one_hot.drop(supermarket_one_hot.filter(regex='_b\'\?\'$',axis=1).columns,axis=1,inplace=True)

# print(supermarket.head())
# pd.set_option('display.max_colwidth',None)

print("#Q2 -------------")

itemsets = apriori(supermarket_one_hot, min_support=0.1, use_colnames=True)
# print(itemsets)


print("#Q3 -------------")

rules = association_rules(itemsets, metric="confidence", min_threshold=0.7)
# print(rules)

print("#Q4 -------------")

# select rules with more than 4 antecedents
rule  = rules.loc[map(lambda x: len(x)==4,rules['antecedents'])]
rule2=rule.loc[map(lambda x: len(x)==1,rule['consequents'])]
# print(rule2)


#Q5 ----------


pd.set_option('display.max_columns', None)
print("#Q6 ----------")

#Q6 ----------

var = "coffee_b't'" and "biscuits_b't'"

sortBy = rules[rules["antecedents"].apply(lambda x: var in str(x))]

print(sortBy.sort_values(by=['confidence']))
